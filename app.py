from fastapi import FastAPI, status,Depends,Response
import models
from pydantic_base_models import User,UserLogin, Contacts
from database import SessionLocal
import bcrypt
from typing import Union
from jwt_handler import signJWT, decodeJWT
from email_validator import validate_email
from fastapi.security import OAuth2PasswordBearer
from sqlalchemy import and_
oauth2_scheme=OAuth2PasswordBearer(tokenUrl='user/login')
app=FastAPI()

import models

models.create_tables()

db=SessionLocal()
# uvicorn app:app --reload


#-------------------------------API 1-----------------------------------------------
   
   
@app.post('/user/signup')
def new_user(user:User, response:Response):
    if user.email==None:
        response.status_code=status.HTTP_400_BAD_REQUEST
        return {
            "message": "Email Field cannot be left blank",
            "data": {}
        }
    if user.password==None:
        response.status_code=status.HTTP_400_BAD_REQUEST
        return {
            "message": "Password Field cannot be left blank",
            "data": {}
        }
    if user.name==None:
        response.status_code=status.HTTP_400_BAD_REQUEST
        return {
            "message": "Name Field cannot be left blank",
            "data": {}
        }
    users=db.query(models.User).all()
    for curr_user in users:
        if curr_user.email==user.email:
            response.status_code=status.HTTP_400_BAD_REQUEST
            return {
                        "message": "Email already registered",
                        "data" : {}
                    }
    if (not validate_email(user.email)):
        response.status_code=status.HTTP_400_BAD_REQUEST
        return {
                    "message": "Email is not valid",
                    "data": {}
                }

    access_token=signJWT(user.email)
    hashed_password=bcrypt.hashpw(user.password.encode("utf-8"),bcrypt.gensalt())
    password_hash=hashed_password.decode("utf8")
    entry = models.User( name=user.name, email=user.email,access_token=access_token,
                        hashed_password=password_hash, phone_number=user.phone_number, address=user.address)
    

    db.add(entry)
    db.commit()
    response.status_code=status.HTTP_200_OK
    user=db.query(models.User).first()
    return {
    "message" : "User signup complete",
    "data": {
        "access_token": access_token,
        "user": {
            "id": user.id,
            "name": user.name,
            "email": user.email
        }
    }
}
    
    
#--------------------------------API 2-----------------------------------------


@app.post("/user/login")
def user_login(user: UserLogin,response=Response):
    users=db.query(models.User).all()
    if (not validate_email(user.email)):
        response.status_code=status.HTTP_400_BAD_REQUEST
        return {
                        "message": "Email is not valid",
                        "data": {}
                    }
    for curr_user in users:
        if curr_user.email==user.email:
            # hashed_password=(bcrypt.hashpw(user.password.encode("utf-8"),bcrypt.gensalt()))
            
            if bcrypt.checkpw(user.password.encode("utf-8"),curr_user.hashed_password.encode("utf-8")):
                access_token=signJWT(user.email)
                response.status_code=status.HTTP_200_OK
                return{
                    "message" : "Login Successful",
                    "data": {
                        "access_token": access_token,
                        "user": {
                            "id": (users[-1].id)+1,
                            "name": curr_user.name,
                            "email": user.email
                        }
                    }}
            response.status_code=status.HTTP_401_UNAUTHORIZED
            return {
                    "message" : "Invalid credentials",
                    "data": {}
                }
    response.status_code=status.HTTP_400_BAD_REQUEST
    return {
                "message": "Email not registered",
                "data": {}
            }
    

#----------------------------------API 3------------------------------------------


@app.post('/user')
def get_user(token:str=Depends(oauth2_scheme),response=Response):
    payload=decodeJWT(token)
    # print(payload)
    if payload==None:
        response.status_code=status.HTTP_400_BAD_REQUEST
        return {
                "message": "Authentication failed",
                "data": {}
            }

    user=db.query(models.User).filter(models.User.email == payload['email']).first()
    # print(user)
    response.status_code=status.HTTP_200_OK
    return {
    "message" : "User detail",
    "data": {
        "id":user.id,
        "name": user.name,
        "email": user.email
            }
        }
    

#------------------------------------API 4---------------------------------------------


    
@app.post('/contact')
def new_contact(contact:Contacts, token:str=Depends(oauth2_scheme),response=Response):
    payload=decodeJWT(token)
    # print(payload)
    if payload==None:
        response.status_code=status.HTTP_400_BAD_REQUEST
        return {
                "message": "Authentication failed",
                "data": {}
            }

    # print(id)
    # print(contact.name, contact.phone)
    if contact.name!=None and contact.phone!=None:
        # print("in if")
        entry = models.Contacts(user_email=payload['email'] ,name=contact.name, email=contact.email,country=contact.country,
                         phone=contact.phone, address=contact.address)
        db.add(entry)
        db.commit()
        # print(entry)
        id=db.query(models.Contacts).all()[-1].id
        response.status_code=status.HTTP_200_OK
        return {
            "message": "Contact added",
            "data": {
                "id": id,
                "name":entry.name,
                "email": entry.email,
                "phone": entry.phone,
                "country": entry.country,
                "address": entry.address
                    }
                }

    else:
        # print("in else")
        response.status_code=status.HTTP_400_BAD_REQUEST
        return{
            "message": "Name and Contact Field cannot be left blank",
            "data": {}
        }
        

# --------------------API 5,6,7 ----------------------------------



@app.get('/contact')
def show_all_contact(page: Union[int,None] = 1,limit: Union[int,None]=10,name: Union[str, None] = None,email: Union[str, None] = None,
                     phone: Union[int, None] = None,sort_by: Union[str, None] = None,token: str = Depends(oauth2_scheme),response=Response):
    payload=decodeJWT(token)
    # print(payload)
    if payload==None:
        response.status_code=status.HTTP_400_BAD_REQUEST
        return {
                "message": "Authentication failed",
                "data": {}
            }
    user_email=payload['email']
    
    sort_query = 'models.Contacts.'
    search_query='.filter_by(user_email=user_email)'
        
    if sort_by == 'alphabetically_a_to_z':sort_query+= 'name'
    elif sort_by == 'alphabetically_z_to_a':sort_query += 'name.desc()'
    elif sort_by == 'latest':sort_query += 'id.desc()'
    elif sort_by == 'oldest':sort_query += 'id'
    else: sort_query +='id'
    
    
    if name:search_query += '.filter(models.Contacts.name.contains(name))'
    if email:search_query += '.filter(models.Contacts.email.contains(email))'
    if phone:search_query += '.filter(models.Contacts.phone.contains(phone))'

    final_query = 'db.query(models.Contacts)'+f'{search_query}'

    if sort_query:   final_query += f'.order_by({sort_query})'
    off=limit*(page-1)
    final_query += f'.offset({off}).limit({limit}).all()'
    contacts = eval(final_query)
    
    listcontact=[]
    for contact in contacts:
            obj={c.name: getattr(contact, c.name) for c in contact.__table__.columns}
            listcontact.append(obj)
    total=len(db.query(models.Contacts).filter_by(user_email=user_email).all())
    pages=(total//limit)+1
    list={'List' : listcontact}
    response.status_code=status.HTTP_200_OK
    return {'message': 'Contact list','data': list,'has_next': page<pages,
    'has_prev': page>1,'page':page,'pages':pages,'per_page':limit,'total':total}
    
        




    
