from pydantic import BaseModel

class User(BaseModel):
    
    name:str=None
    phone_number:int=None
    password:str=None
    email:str=None
    address:str=None
    class Config:
        orm_mode=True

class UserLogin(BaseModel):
    email: str=None
    password: str =None
    class Config:
        orm_mode=True
class Contacts(BaseModel):
    
    name:str=None
    email:str=None
    phone:int=None
    address:str=None
    country:str=None
    class Config:
        orm_mode=True
