import time, jwt
#from decouple import config
from fastapi.security import OAuth2PasswordBearer

import os
JWT_SECRET= "5ddf6ee986554f0d168ddba7cc23cf45"
# os.environ.get("SECRET_KEY")
JWT_ALGORITHM='HS256'
# os.environ.get('jwtalgoritm')


def signJWT(email:str):
    payload={
        "email":email,
        "expiry":time.time()+60*60*24
    }
    token=jwt.encode(payload,JWT_SECRET, algorithm=JWT_ALGORITHM)
    return (token)
def decodeJWT(token:str):
    try:
        decode_token=jwt.decode(token,JWT_SECRET, algorithm=JWT_ALGORITHM)
        # print(decode_token)
        return decode_token if decode_token['expiry']>=time.time() else None
    except:
        return None
