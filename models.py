from database import Base, engine
from sqlalchemy import DateTime, Text, Identity,Column, BigInteger, ForeignKey
from sqlalchemy.sql import func
from sqlalchemy.orm import relationship

def create_tables():
    Base.metadata.create_all(engine)

class User(Base):
    __tablename__='user'
    id=Column(BigInteger, Identity(start=1, cycle=True), nullable=False)
    access_token = Column(Text,nullable=False) 
    hashed_password=Column(Text,  nullable=False) 
    name=Column(Text,  nullable=False) 
    phone_number=Column(Text,  nullable=True)
    # phone_number=Column(BigInteger,  nullable=True)
    email=Column(Text,  nullable=False, primary_key=True) 
    address=Column(Text,  nullable=True) 
    contacts_rel=relationship("Contacts", back_populates="user_rel") 
    
class Contacts(Base):
    __tablename__='contacts'
    id=Column(BigInteger,   Identity(start=1, cycle=True), nullable=False, primary_key=True) 
    # user_id = Column(BigInteger, ForeignKey('user.id'), nullable=False)
    user_email = Column(Text, ForeignKey('user.email'), nullable=False)
    name=Column(Text,  nullable=False) 
    email=Column(Text,  nullable=True) 
    phone=Column(Text,  nullable=True)
    # phone=Column(BigInteger,  nullable=False) 
    address=Column(Text,  nullable=True) 
    country=Column(Text,  nullable=True) 
    user_rel=relationship("User", back_populates="contacts_rel")
