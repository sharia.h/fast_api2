from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
import os
# os.environ.get('A10_DB_URI')
SQLALCHEMY_DATABASE_URI='postgresql://postgres:sharia28@localhost:5432/userdetail'

engine=create_engine(SQLALCHEMY_DATABASE_URI)
SessionLocal=sessionmaker(bind=engine)
Base=declarative_base()
